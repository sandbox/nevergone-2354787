core = "7.x"
api = "2"


; Contrib modules
projects[admin_menu][subdir] = "dev_tools"
projects[admin_menu][version] = "3.0-rc5"

projects[advanced_help][subdir] = "dev_tools"
projects[advanced_help][version] = "1.3"

projects[coder][subdir] = "dev_tools"
projects[coder][version] = "2.6"

projects[devel][subdir] = "dev_tools"
projects[devel][download][branch] = "7.x-1.x"
projects[devel][download][revision] = "496880dc"

projects[devel_contrib][subdir] = "dev_tools"
projects[devel_contrib][version] = "1.1"

projects[diff][subdir] = "dev_tools"
projects[diff][version] = "3.3"

projects[environment_indicator][subdir] = "dev_tools"
projects[environment_indicator][version] = "2.9"

projects[filter_perms][subdir] = "dev_tools"
projects[filter_perms][version] = "1.0"

projects[git_deploy][subdir] = "dev_tools"
projects[git_deploy][download][branch] = "7.x-2.x"
projects[git_deploy][download][revision] = "36d886f5"

projects[masquerade][subdir] = "dev_tools"
projects[masquerade][version] = "1.0-rc7"

projects[module_filter][subdir] = "dev_tools"
projects[module_filter][version] = "2.1"

projects[search_krumo][subdir] = "dev_tools"
projects[search_krumo][version] = "1.6"

projects[stage_file_proxy][subdir] = "dev_tools"
projects[stage_file_proxy][version] = "1.7"

projects[style_usage][subdir] = "dev_tools"
projects[style_usage][version] = "1.1"

projects[styleguide][subdir] = "dev_tools"
projects[styleguide][version] = "1.1"
